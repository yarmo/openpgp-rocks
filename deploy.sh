# Remove the current public folders
rm -rf ./public ./public_git

# Clone the pages repository
git clone -b pages git@codeberg.org:yarmo/openpgp-rocks.git public_git

# Build the website
zola build

# Add the .domains file
cp .domains ./public/

# Sync the freshly built website with the versioned website
rsync -a ./public/ ./public_git/

# Upload to the pages repository
git -C public_git add -A
git -C public_git commit -m "Website update"
git -C public_git push

# Clean up
rm -rf ./public ./public_git